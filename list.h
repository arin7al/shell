#ifndef LIST_H
#define LIST_H

typedef struct list_word {
    char *str;
    struct list_word *next;
} list;

//extern jmp_buf begin;

list* addlist(list*, list*);
list* cleanlist(list *);
void printlist(list *);

list* separate(char*);
char* readlist(void);

#endif
