#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <setjmp.h>
#include <sys/wait.h>
#include <pwd.h>	
#include <limits.h>

#include "tree.h"

int is_symbol(char *plex) {
	char a1[]="&&";
	char a2[]="||";
	char a3[]="&";
	char a4[]="|";
	char a5[]=">";
	char a6[]=">>";
	char a7[]="<";
	char a8[]=";";
	return (!comparestr(plex ,a1) || !comparestr(plex ,a2) || !comparestr(plex, a3) || !comparestr(plex, a4) || !comparestr(plex, a5) || !comparestr(plex, a6) || !comparestr(plex, a8) || !comparestr(plex, a7));
}

int comparestr(char*s1,char*s2) {           //выводит 0, если строки одинаковые
    for(;(*s1==*s2 && *s1!='\0');s1++,s2++);
    return *s1-*s2;
}

list*last(list*l){
  while(l->next)
    l=l->next;
  return l;
}

int exits(list*spisok){
  while(spisok){
    if (!strcmp(spisok->str,"exit"))
      return 1;
    else spisok=spisok->next;
  }
  return 0;
}

void cleantree(tree sh) {
		int i;

	    for (i = 0; i < sh -> argc; i++) {
	    	free(sh -> arg[i]);
	    }
	    free(sh -> arg);

	    if (sh -> infile != NULL) 
	    	free(sh -> infile);
	    if (sh -> outfile != NULL)
	    	free(sh -> outfile);
	    if (sh -> append != NULL)
	    	free(sh -> append);

	    if (sh->next != NULL)
	    	cleantree(sh->next);
	    else if (sh->pipe != NULL)
	    	cleantree(sh->pipe);
	    else if (sh->or != NULL)
	      	cleantree(sh->or);
	    else if (sh->and != NULL)
	      	cleantree(sh->and);

	    free(sh);
}

void print_tree(tree sh){

  int i;
  //printf("*\n");
  if (sh!=NULL){
  	printf("Arguments: \n");
    for (i=0;sh->arg[i]!=NULL;i++){
    	printf("-\n");
    	printf("%s\n",sh->arg[i]);
    }
    printf("background: \n");
    printf("%d\n",sh->backgrnd);

    
    if(sh->outfile!=NULL) {
    	printf("%s\n",sh->outfile);
    	printf("outfile\n");
	}
    if(sh->infile!=NULL) {
    	printf("%s\n",sh->infile);
    	printf("infile\n");
    }
    
    if(sh->append!=NULL) {
    	printf("%s\n",sh->append);
    	printf("append\n");
    }
    
    print_tree(sh->pipe);
    print_tree(sh->next);
    print_tree(sh->and);
    print_tree(sh->or);
  }
}      

void make_bgrnd(tree t) {
	t -> backgrnd = 1;			//ИСПРАВИТЬ/дополнить
}

tree make_cmd(char *l) {
	tree t;

	t = (struct cmd_inf *)malloc(sizeof(struct cmd_inf));

	t -> infile = NULL;
	t -> outfile = NULL;
	t -> append = NULL;
	t -> backgrnd = 0;
	t -> argc = 1;

	t -> pipe = NULL;
	t -> next = NULL;
	t -> and = NULL;
	t -> or = NULL;

	t -> arg = malloc(sizeof(char*)*2);
	t -> arg[0] = malloc((strlen(l) + 1)*sizeof(char));
	t -> arg[0] = strcpy(t -> arg[0], l);					
	t -> arg[1] = NULL;
	return t;
}

tree add_arg(tree cur_cmd, char *l) {
	(cur_cmd->argc)++;
	cur_cmd->arg = (char**)realloc(cur_cmd->arg, (cur_cmd->argc + 1)*sizeof(char*));
	cur_cmd->arg[cur_cmd->argc-1] = malloc((strlen(l) + 1)*sizeof(char));
	cur_cmd->arg[cur_cmd->argc-1] = strcpy(cur_cmd->arg[cur_cmd->argc-1], l);         //добавляем очередной аргумент в список аргументов	
	cur_cmd->arg[cur_cmd->argc] = NULL;
	return cur_cmd;
}

tree build_tree(list *plex) {

	tree beg_cmd; /* начальная команда, это локальная переменная функции build_tree()*/
	tree cur_cmd; /* текущая команда, локальная переменная функции build_tree()*/
	tree prev_cmd; /* предыдущая команда, тоже локальная */

	char a1[]="&&";
	char a2[]="||";
	char a3[]="&";
	char a4[]="|";
	char a5[]=">";
	char a6[]=">>";
	char a7[]="<";
	char a8[]=";";

	//обработка 1 поманды в шелле
	if (!is_symbol(plex -> str)) {		
		beg_cmd = make_cmd(plex -> str);
		cur_cmd = beg_cmd;
		prev_cmd = cur_cmd;
		plex = plex -> next;
	}
	else {}
		//SYNTAX ERROR 

	conv:
	
	if (plex == NULL) {
		return beg_cmd;
	}
	//аргумент
	if (!is_symbol(plex -> str)) {
		cur_cmd = add_arg(cur_cmd, plex->str);
		plex = plex -> next;
		goto conv;
	}	

	// |
	if (!comparestr(plex->str,a4)) { // |
		plex = plex -> next;
		if (!is_symbol(plex -> str)) {
			prev_cmd = cur_cmd;
			cur_cmd = make_cmd(plex -> str);
			prev_cmd -> pipe = cur_cmd;
			plex = plex -> next;
		}
		else {}
			//SYNTAX ERROR 
		goto conv;
	}

	// <
	if (!comparestr(plex->str,a7)) { 
		plex = plex -> next;
		cur_cmd -> infile = malloc((strlen(plex -> str) + 1)*sizeof(char));
		cur_cmd -> infile = strcpy(cur_cmd -> infile, plex -> str);
		plex = plex -> next;
		if ((!comparestr(plex->str,a5)) || (!comparestr(plex->str,a6))) { //> >>
			if (!comparestr(plex->str,a6)) {
				plex = plex -> next;
				cur_cmd -> append = malloc((strlen(plex -> str) + 1)*sizeof(char));
				cur_cmd -> append = strcpy(cur_cmd -> append ,plex -> str);
				plex = plex -> next;
			}
			else {
				plex = plex -> next;
				cur_cmd -> outfile = malloc((strlen(plex -> str) + 1)*sizeof(char));
				cur_cmd -> outfile = strcpy(cur_cmd -> outfile, plex -> str);
				plex = plex -> next;
			}			
		}
		goto conv;
	}

	// >	>>
	if ((!comparestr(plex->str,a5)) || (!comparestr(plex->str,a6))) {

		if (!comparestr(plex->str,a6)) {
			plex = plex -> next;
			cur_cmd -> append = malloc((strlen(plex -> str) + 1)*sizeof(char));
			cur_cmd -> append = strcpy(cur_cmd -> append ,plex -> str);
			plex = plex -> next;
		}
		else {
			plex = plex -> next;
			cur_cmd -> outfile = malloc((strlen(plex -> str) + 1)*sizeof(char));
			cur_cmd -> outfile = strcpy(cur_cmd -> outfile, plex -> str);
			plex = plex -> next;		
		}
		goto conv;
	}

	// ;	&
	if ((!comparestr(plex->str,a8)) || (!comparestr(plex->str,a3))) {
		if (!comparestr(plex->str,a3)) {
			make_bgrnd(cur_cmd);
		}
		plex = plex -> next;
		if (plex == NULL) {
			return beg_cmd;
		}
		else {
			prev_cmd = cur_cmd;
			prev_cmd -> next = build_tree(plex);			//????????????		
		}
	}
	//и та же фигня с || и &&
	if (!comparestr(plex->str,a1)) {		//  &&
		plex = plex -> next;
		if (plex == NULL) {
			return beg_cmd;
		}
		else {
			prev_cmd = cur_cmd;
			prev_cmd -> and = build_tree(plex);	
		}
	}
	if (!comparestr(plex->str,a2)) {		//  ||
		plex = plex -> next;
		if (plex == NULL) {
			return beg_cmd;
		}
		else {
			prev_cmd = cur_cmd;
			prev_cmd -> or = build_tree(plex);	
		}
	}
	return beg_cmd; //NADO LI
}

