#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <setjmp.h>
#include <sys/wait.h>
#include <pwd.h>	
#include <limits.h>

#include "list.h"				//????


list* cleanlist(list *list1) {
	list *temp;
	while (list1 != NULL) {
		temp = list1;
		temp -> next = NULL;
		list1 = list1 -> next;
		free(temp -> str);
		free(temp);
	}
	return NULL;
}

//обычный ли это имвол
int checkchar(char c) {
	char wordchars[]= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_./$";
	if (strrchr(wordchars, c) != NULL)
		return 1;
	else 
		return 0;
}
//специальный ли это символ
int checkspec(char c) {
	char wordchars[]= "|&;><,()";
	if (strrchr(wordchars, c) != NULL)
		return 1;
	else 
		return 0;
}
//пробельный ли это символ
int checkprob(char c) {
	if ((c == ' ') || (c == '\t') || (c == '\n'))
		return 1;
	else 
		return 0;
}
//выдает длину без 0
int strlenn(char *str) {
	int length = 0;	
	while (*str++) {
		length++;
	}
	return length;
}
//копирует строку без 0
void copystr(char *str1, char *str2) {
	int i;
	for ( i=0 ; i < strlenn(str2); i++) {
		str1[i] = str2[i];
	}
	return;
}


//выводит на печать список
void printlist(list *list1) {
	while (list1 != NULL) {
		printf("%s\n", list1 -> str);
		list1 = list1 -> next;
	}
	return;
}

// разделет строку на список слов
list *separate(char *string) {
	char c;
	char *curelem;
	list *list1, *list2;
	list1 = malloc (sizeof(list));
	list2 = list1;
	int i = 0, curlen = 0;

	while ((c = string[i]) != '\0') {
		if (checkchar(c) == 1) {
			curelem = malloc (sizeof(c)*2);
			curelem[curlen] = c; 			//помещаем туда считанную букву
			++curlen;
			while (checkchar(string[i+1])== 1) {	//считываем все слово до конца
				++i;
				++curlen;
				curelem = realloc( curelem, sizeof(c)*curlen + 1);
				curelem[curlen-1] = string[i]; 
			}
			//addword(list2, curelem);
			curelem[curlen] = '\0';

			list2 -> next = malloc (sizeof(list));	
			list2 = list2->next;	
							// list2 -> next = NULL;	
			list2 -> str = malloc(strlenn(curelem));
			list2 -> next = NULL;
			copystr(list2 -> str, curelem);
			//printf("%s\n", list2 -> elem);

			free(curelem);
			curlen = 0;
			++i;
		} 
		else if (checkspec(c) == 1) {
			if (checkspec(string[i+1])==0) { //если следующий не специальный
				curelem = malloc (sizeof(c)*2);
				curelem[0] = c;
				curelem[1] = '\0';
				//printf("%s\n", curelem);
				//addword(list2, curelem);
				

				list2 -> next = malloc (sizeof(list));	
				list2 = list2->next;	
								// list2 -> next = NULL;	
				list2 -> str = malloc(sizeof(c)*2);
				list2 -> next = NULL;
				copystr(list2 -> str, curelem);
				//printf("%s\n", list2 -> elem);

				free(curelem);
			}
			else {						//если следующий тоже специальный
										//проверяем, составной ли	
				if ((string[i+1] == c) && ((c == '&') || (c == '|') || (c == '>'))) {
					++i;				//считываем 
					curelem = malloc(sizeof(c)*3);
					curelem[0] = c;
					curelem[1] = c;
					curelem[2] = '\0';
					//addword(list2, curelem);


					list2 -> next = malloc (sizeof(list));	
					list2 = list2->next;	
									// list2 -> next = NULL;	
					list2 -> str = malloc(sizeof(c)*3);
					list2 -> next = NULL;
					copystr(list2 -> str, curelem);
					//printf("%s\n", list2 -> elem);

					free(curelem);
				}
				else {
					curelem = malloc (sizeof(c)*2);
					curelem[0] = c;
					curelem[1] = '\0';
					//addword(list2, curelem);

					list2 -> next = malloc (sizeof(list));	
					list2 = list2->next;	
									// list2 -> next = NULL;	
					list2 -> str = malloc(sizeof(c)*2);
					list2 -> next = NULL;
					copystr(list2 -> str, curelem);
					//printf("%s\n", list2 -> elem);

					free(curelem);
				}	
			}		
			++i;	
		}
		else if (checkprob(c)) {
			++i;
		}
		else {
			if (c == EOF) {
				break;
			}
			printf( "Что-то пошло не так\n" );
			++i;
		}
	}
	list2 -> next = NULL;
	list1 = list1 -> next;
	return (list1);
}


char* readlist() {
	char buf[10];
	char *strin;
	char*s=NULL;
	strin=(char*)malloc(sizeof(char));
  int b=1;
  strin[0]='\0';

  while ((s=fgets(buf,sizeof(buf),stdin))!=NULL){
    strin=(char*)realloc(strin,b*sizeof(buf));
    if (strin!=NULL){
      strin=strcat(strin,buf);
      b++;
    }
    if(strchr(strin,'\n')!=NULL)
      break;        
  }
   return strin;
}

