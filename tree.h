#ifndef TREE_H
#define TREE_H

#include "list.h"

struct cmd_inf {
	char ** arg; /* список из имени команды и аргументов */
	int argc;	/* количество аргументов */
	char *infile; /*     <     переназначенный файл стандартного ввода */
	char *outfile; /*    > >>  переназначенный файл стандартного вывода */
	char *append;
	
	int backgrnd; /* =1, &   если команда подлежит выполнению в фоновом режиме */ 
	
	struct cmd_inf* pipe; /* следующая команда после “|” */
	struct cmd_inf* next; /* следующая команда после “;” или "&" */
	struct cmd_inf* and; /* следующая команда после “&&” */
	struct cmd_inf* or; /* следующая команда после “||” */
};

typedef struct cmd_inf *tree;

//extern jmp_buf begin;
//extern list *plst;

int comparestr(char *, char *);
list *last(list *);
int exits(list*);
void print_tree(tree);
tree build_tree();
void cleantree(tree);
tree make_cmd(); /* создает дерево из одного элемента, обнуляет все поля */
void make_bgrnd(tree t); /* устанавливает поле backgrnd=1 во всех командах конвейера t */ 
tree add_arg(); /* добавляет очередной элемент в массив arg текущей команды  argc++ */
int is_symbol(char *);

list *plex; /* указатель текущей лексемы, начальное значение передается через параметр функции build_tree(), список list – это массив указателей на лексемы-строки */
tree beg_cmd; /* начальная команда, это локальная переменная функции build_tree()*/
tree cur_cmd; /* текущая команда, локальная переменная функции build_tree()*/
tree prev_cmd; /* предыдущая команда, тоже локальная */

/*void err_file();
void error(char *, char *); */


#endif
