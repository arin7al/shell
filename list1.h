#ifndef LIST_H
#define LIST_H

typedef struct list_word {
    char *str;
    struct list_word *next;
} list;


list* cleanlist(list *);
int checkchar(char);
int checkspec(char);
int checkprob(char);
int strlenn(char *);
void copystr(char *, char *);
void printlist(list *);
list *separate(char *);
char* readlist(void);

#endif


