#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <setjmp.h>
#include <sys/wait.h>
#include <pwd.h>	
#include <limits.h>

#include "list.h"
#include "tree.h"
#include "execute.h"

int z=0;
int*zombie=NULL;
char *HOME; /* домашняя директория пользователя */

//char SHELL[PATH_MAX+1]; /* путь к исполняемому в данный момент шеллу */
char *SHELL;
char *USER; /* имя пользователя, запустившего процесс */
/*uid_t  EUID;  идентификатор пользователя, с правами которого работает процесс */

char *EUID;
uid_t  EUID1;

list* change(list*spisok,char*p,char*s){
  list*l=spisok;
  while(spisok!=NULL){
    if (!strcmp(spisok->str,p)){
      spisok->str=(char*)realloc(spisok->str,(strlen(s)+1)*sizeof(char));
      strcpy(spisok->str,s);
    }
    spisok=spisok->next;
  }
  return l;
}

int main(int argc, char **argv) {

	tree k;

    char*home="$HOME";
    char*user="$USER";
    char*sel="$SHELL";
    char*euid="$EUID";

	signal(SIGINT,handler);

  list *spisok=NULL;
  char *strin=NULL;
  printf("WELCOME TO MY SHELL!\n");

while (2>1) {

  	printf("myshell$ ");
  	spisok=NULL;
  	strin = readlist();
  	spisok=separate(strin);


    int a,w,j,t=z;
    for(j=0;j<z;j++){
      if  ( ( w=waitpid(zombie[j],NULL,WNOHANG))!=0){ //если таких нет, то и не ждет
        waitpid(zombie[j],NULL,WUNTRACED);  //замечает также остановивш процессы
        for( a=j;a<=z;a++)
         zombie[a]=zombie[a+1];
        t--;
        fprintf(stderr,"%d\n",w);
      }
    }

    z=t;

    if (spisok==NULL) goto finish;

  /* Заполнение переменных окружения */
    HOME = getenv("HOME");
    SHELL = getenv("SHELL");
    USER = getenv("USER");
    //EUID1 = geteuid();
    //itoa(EUID1, EUID);

    spisok=change(spisok,home,HOME);
    spisok=change(spisok,user,USER);
    spisok=change(spisok,sel,SHELL);
    spisok=change(spisok,euid,EUID);

    if (exits(spisok)) {              //если встретился exit чистим все и завершаемся
      spisok=cleanlist(spisok);
      free(strin);
      return 1;
    }

  	k = build_tree(spisok);	
  	//print_tree(k);

  	next(k);				

      /*while((w=wait(NULL))!=-1){
        fprintf(stderr,"%d\n",w);
        printf("команда в фоновом режиме закончила работу\n");
      }*/

    cleantree(k);
    er:    
    spisok=cleanlist(spisok);
    
    finish:
    free(strin);
  }
  //while(s!=NULL);
    free(zombie);


 	return 0;
}



