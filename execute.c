#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <setjmp.h>
#include <sys/wait.h>
#include <pwd.h>	
#include <limits.h>
#include <errno.h>


#include "execute.h"
#include "tree.h"
#include "list.h"	//????

void handler(){}
void handler2() {wait(NULL);}

int back(tree sh){
  while(sh->pipe)
    sh=sh->pipe;
  return sh->backgrnd;
}

tree pipess(tree sh){
  int fd[2],in,out,next_in,fdin,fdout,fdapp,i=0,j=0;
  int status;
  int *pid;
  


  if (sh==NULL) 
    return sh;
  if (sh->pipe==NULL)
    return sh;

  pid=(int*)malloc(sizeof(int));

  pipe(fd); out=fd[1]; next_in=fd[0];
  i++;

  if(!strcmp(sh->arg[0],"cd")){
    if (sh->arg[1]!=NULL)
      chdir(sh->arg[1]);
    else (chdir(HOME));
  }

  else
    if ((pid[j]=fork())==0){  //СЫН
      dup2(out,1);
      if(sh->infile!=NULL){ 
        fdin=open(sh->infile,O_RDONLY);  //открываем infile
        if (fdin!=-1){
          dup2(fdin,0);
          close(fdin);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }
      if(sh->outfile!=NULL){
        fdout=open(sh->outfile,O_WRONLY|O_CREAT|O_TRUNC,0666);                 //открываем outfile
        if(fdout!=-1) {
          dup2(fdout,1);
          close(fdout);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }
      if(sh->append!=NULL){                         
        fdapp=open(sh->append,O_WRONLY|O_APPEND|O_CREAT,0666);  //открываем append
        if(fdapp!=-1){
          dup2(fdapp,1);
          close(fdapp);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }/**/
      /*dup2(out,1);*/
      if(back(sh)==1)
        signal(SIGINT,SIG_IGN);       //если в фоновом режиме, то игнорим сигнал
      close(next_in);
      close(out);
      execvp(sh->arg[0],sh->arg);     //запускаем команду
      fprintf(stderr,"команда не найдена\n");
      exit(-1);
    } //КОНЕЦ СЫНА

  in=next_in;
  while(sh->pipe->pipe!=NULL){
    j++;
    pid=(int*)realloc(pid,(j+1)*sizeof(int));
    i++;
    close(out);
    pipe(fd);
    out=fd[1];
    next_in=fd[0];
  /*if(!strcmp(sh->arg[0],"cd")){
     if (sh->arg[1]!=NULL)
 chdir(sh->arg[1]);
     else (chdir(HOME));
  }
  else*/
    if ((pid[j]=fork())==0){  //ЕЩЕ СЫН
    	close(next_in);
    	dup2(in,0);
    	close(in);
    	dup2(out,1);
      if(sh->pipe->infile!=NULL){
        fdin=open(sh->pipe->infile,O_RDONLY); //открываем infile
        if (fdin!=-1){
          dup2(fdin,0);
          close(fdin);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }

      if(sh->pipe->outfile!=NULL){
        fdout=open(sh->pipe->outfile,O_WRONLY|O_CREAT|O_TRUNC,0666);             //открываем outfile
        if(fdout!=-1){
          dup2(fdout,1);
          close(fdout);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }
      if(sh->pipe->append!=NULL){
        fdapp=open(sh->append,O_WRONLY|O_APPEND|O_CREAT,0666);  //открываем append
        if(fdapp!=-1){
          dup2(fdapp,1);
          close(fdapp);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }/**/
/*close(next_in);
dup2(in,0);
close(in);
dup2(out,1);*/
      if(back(sh->pipe)==1)
        signal(SIGINT,SIG_IGN);             //если в фоновом режиме, то игнорим сигнал
    	close(out);
    	execvp(sh->pipe->arg[0],sh->pipe->arg);  //запускаем команду
    	fprintf(stderr,"команда не найдена\n");
    	exit(-1);
    }  //КОНЕЦ СЫНА

    close(in);
    in=next_in;
    sh=sh->pipe;
  }//конец WHILE


  close(out);
  sh=sh->pipe;                                 //обрабатываем последнюю команду
  i++;
  j++;
  pid=(int*)realloc(pid,(j+1)*sizeof(int));
  /*if(!strcmp(sh->arg[0],"cd")){
      if (sh->arg[1]!=NULL)
	chdir(sh->arg[1]);
      else (chdir(HOME));
    }
    else*/
  if ((pid[j]=fork())==0){
    dup2(in,0);
    if(sh->infile!=NULL){
      fdin=open(sh->infile,O_RDONLY);
      if (fdin!=-1){
        dup2(fdin,0);
        close(fdin);
      }
      else fprintf(stderr,"ошибка с файлом\n");
    }    
    if(sh->outfile!=NULL){
      fdout=open(sh->outfile,O_WRONLY|O_CREAT|O_TRUNC,0666);
      if(fdout!=-1){
        dup2(fdout,1);
        close(fdout);
      }
      else fprintf(stderr,"ошибка с файлом\n");
    }
    if(sh->append!=NULL){
      fdapp=open(sh->append,O_WRONLY|O_APPEND|O_CREAT,0666);
      if(fdapp!=-1){
  	    dup2(fdapp,1);
  	    close(fdapp);
      }
      else fprintf(stderr,"ошибка с файлом\n");
    }/**/
	/*dup2(in,0);*/
    if(back(sh)==1)
      signal(SIGINT,SIG_IGN);
  	close(in);
  	execvp(sh->arg[0],sh->arg);
  	fprintf(stderr,"команда не найдена\n");
  	exit(-1);
	
  }
  close(in);
  if (sh->backgrnd==0){              //если не в фоновом режиме
    for(i=0;i!=j+1;i++){  
      waitpid(pid[i],&status,WUNTRACED);
    }
    if (sh->and!=NULL){
      if (WIFEXITED(status)==0 || WEXITSTATUS(status)!=0){
        sh=NULL;
        goto fin;
      }
      sh=sh->and;
    }
    else
      if (sh->or!=NULL){
        if (WIFEXITED(status)!=0 && WEXITSTATUS(status)==0){
          sh=NULL;
          goto fin;
        }
        sh=sh->or;
      }
    else sh=sh->next;
  }

  else {                            //если в фоновом режиме
    sh=sh->next;
    zombie=(int*)realloc(zombie,(z+j+2)*sizeof(int));
    for(i=z;i!=z+j+1;i++)
      zombie[i]=pid[i-z];
    z=z+j+1;
  }

  fin:
  free(pid);
  return sh;
}

void next(tree sh){
  /*printshell(sh);*/
  int status;
  int fdin,fdout,fdapp,pid;

  sh=pipess(sh);

  if (sh!=NULL){

    if(!strcmp(sh->arg[0],"cd")){
      if (sh->arg[1]!=NULL)
        chdir(sh->arg[1]);
      else (chdir(HOME));
    }

    else if ((pid=fork())==0){
      if(sh->backgrnd==1)
        signal(SIGINT,SIG_IGN);

      if (sh->infile!=NULL){
        fdin=open(sh->infile,O_RDONLY);
        if (fdin!=-1){
          dup2(fdin,0);
          close(fdin);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }

      if(sh->outfile!=NULL){
        fdout=open(sh->outfile,O_WRONLY|O_CREAT|O_TRUNC,0666);
        if(fdout!=-1){
  	      dup2(fdout,1);
  	      close(fdout);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }

      if(sh->append!=NULL){
        fdapp=open(sh->append,O_WRONLY|O_APPEND|O_CREAT,0666);
        if(fdapp!=-1){
          dup2(fdapp,1);
          close(fdapp);
        }
        else fprintf(stderr,"ошибка с файлом\n");
      }


      execvp(sh->arg[0],sh->arg);
      fprintf(stderr,"команда не найдена\n");
      exit(-1);
    }
    
    if (sh->backgrnd==0){
      waitpid(pid,&status,WUNTRACED);
      if (sh->and!=NULL){
        if (WIFEXITED(status)!=0 && WEXITSTATUS(status)==0){
	/*sh=pipess(sh->and);*/
          next(sh->and);
        }
      }
      else
        if (sh->or!=NULL){
          if (WIFEXITED(status)==0 || WEXITSTATUS(status)!=0){
	         /*sh=pipess(sh->or);*/
            next(sh->or);
          }
        }
      else
        next(sh->next);
    }
    else {
      zombie=(int*)realloc(zombie,(z+1)*sizeof(z));
      zombie[z]=pid;
      z++;
      next(sh->next);
    }
  }

}





