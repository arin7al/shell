#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <setjmp.h>
#include <sys/wait.h>
#include <pwd.h>	
#include <limits.h>

#include "list.h"				//????


list* addlist(list*l,list*l1){
  list *p;
  p=l;
  if (l==NULL)
    l=l1;
  else {
	  while (l->next!=NULL)
	    l=l->next;
	  l->next=l1;
	  l=p;
  }
  return l;
}

list* cleanlist(list *l) {
  list *p;
  while (l!=NULL){
  	p=l;
  	l=l->next;
  	p->next=NULL;
  	free(p->str);
  	free(p);
   }
  return NULL;
}

void printlist(list *l){
  while (l!=NULL){
    if (l->str!=NULL)
    printf("%s%s",l->str,"\n");
    l=l->next;
  }
}

list*separate(char*strin){

  int i=0;
  int j;
  char c;
  char*p=NULL;
  list* l1,*spisok=NULL;
  int k,flag=0;

  start:

  while ((c=strin[i])!='\0') {
    j=0;
    switch (c){
	    case '(':case')':case'&':case';':case'>':case'<':case'|':
	            goto specsymbls;
	    case 'q':case'w':case'e':case'r':case't':case'y':case'u':
	    case 'i':case'o':case'p':case'a':case's':case'd':case'f':
	    case 'x':case'z':case'l':case'k':case'j':case'h':case'g':
	    case 'c':case'v':case'b':case'n':case'm':case'1':case'2':
	    case '9':case'8':case'7':case'6':case'5':case'4':case'3':
	    case '0':case'/':case'.':case'_':case'Q':case'W':case'E':
	    case 'P':case'O':case'I':case'U':case'Y':case'T':case'R':
	    case 'A':case'S':case'D':case'F':case'G':case'H':case'J':
	    case 'B':case'V':case'C':case'X':case'Z':case'L':case'K':
	    case 'N':case'M':case '-':
	            goto normsymbols;
	    case ' ':case'\t':case'\n': {i++; break;}
	    default: goto error1;
    }
  }

  goto finish;

  specsymbls: 

  i++;
  if (c==strin[i] && (strin[i]=='|' || strin[i]=='&' || strin[i]=='>')){
    p=(char*) malloc(5*sizeof(char));
    p[0]=c;
    p[1]=strin[i];
    p[2]='\0';
    i++;
  }
  else {
    p=(char*) malloc(5*sizeof(char));
  	p[0]=c;
  	p[1]='\0';
  }
  l1=(list*) malloc(sizeof(list));
  l1->str=p;
  l1->next=NULL;
  spisok = addlist(spisok,l1);
  goto start;

  normsymbols:

  j++;		//количество букв в норм слове
  i++;
  switch (strin[i]) {
	  case 'q':case'w':case'e':case'r':case't':case'y':case'u':
	  case 'i':case'o':case'p':case'a':case's':case'd':case'f':
	  case 'x':case'z':case'l':case'k':case'j':case'h':case'g':
	  case 'c':case'v':case'b':case'n':case'm':case'1':case'2':
	  case '9':case'8':case'7':case'6':case'5':case'4':case'3':
	  case '0':case'/':case'.':case'_':case'Q':case'W':case'E':
	  case 'P':case'O':case'I':case'U':case'Y':case'T':case'R':
	  case 'A':case'S':case'D':case'F':case'G':case'H':case'J':
	  case 'B':case'V':case'C':case'X':case'Z':case'L':case'K':
	  case 'N':case'M':case '-': goto normsymbols;
	  default: {i--;j--; break;}
  }
	i=i-j;
  
	p=(char*)malloc((j+2)*sizeof(char));
	k=0;

	while(k<=j)	{
	  p[k]=strin[i];
	  i++;
	  k++;
	}	
	p[k]='\0';

	l1=(list*) malloc(sizeof(list));
	l1->str=p;
	l1->next=NULL;
	spisok = addlist(spisok,l1);
	/*     	printf("%s",spisok->str);*/
  goto start;

 	error1:
	fprintf(stderr,"ошибка синтаксиса. нельзя вводить этот символ %c%s",c,"\n");
	goto error3;
	//error2:
	//fprintf(stderr,"ошибка синтаксиса, много лишних специальных символов\n");
 	error3:
	i++;
	free(strin);
	spisok=cleanlist(spisok);

  finish:
	return spisok;
}

char* readlist() {
	char buf[10];
	char *strin;
	char*s=NULL;
	strin=(char*)malloc(sizeof(char));
  int b=1;
  strin[0]='\0';

  while ((s=fgets(buf,sizeof(buf),stdin))!=NULL){
    strin=(char*)realloc(strin,b*sizeof(buf));
    if (strin!=NULL){
      strin=strcat(strin,buf);
      b++;
    }
    if(strchr(strin,'\n')!=NULL)
      break;        
  }
   return strin;
}





