CC = gcc
CFLAGS = -g -Wall
PROG = shell

all: $(PROG)

list1.c: list.h

tree.c: tree.h

list.o: list.c list.h
	$(CC) $(CFLAGS) -c $< -o $@
tree.o: tree.c tree.h
	$(CC) $(CFLAGS) -c $< -o $@
execute.o: execute.c execute.h
	$(CC) $(CFLAGS) -c $< -o $@
$(PROG): main.c list.o tree.o execute.o
	$(CC) $(CFLAGS) $< list.o tree.o execute.o -o $(PROG)
clean:
	rm -f *.o $(PROG)
run:
	rlwrap ./$(PROG)